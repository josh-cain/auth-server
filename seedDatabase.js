'use strict'

const log = require('./logger');

module.exports.init = (context, callback) => {
    log.debug('seeding database...')
    var user = new context.app.User({ id: 1, username: 'hank', password: 'propane' });
    user.save(function (err) {
        if (err) { log.error('Unable to seed database with test user!'); }
        var client = new context.app.Client({ client_id: 'test-client-1', name: 'Test Client 1' });

        client.save((err) => {
            if (err) { log.error('Unable to seed database with test client!'); }
            log.debug('database populated with initial seed values.');
            callback(context);
        })
    });
}