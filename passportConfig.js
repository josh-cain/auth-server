'use strict'

const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const GitHubStrategy = require('passport-github').Strategy;
const ClientPasswordStrategy = require('passport-oauth2-client-password');
const BearerStrategy = require('passport-http-bearer');

const log = require('./logger');

module.exports.init = function (context, callback) {
  log.debug('Initializing Passport.js config...');
  if (!context.app || !context.passport) {
    throw new Error('could not initialize passport, context app or passport elements not present');
  }

  // user serialization
  context.passport.serializeUser(function (user, done) {
    log.debug('serializing user: ' + JSON.stringify(user));
    done(null, user._id);
  });
  context.passport.deserializeUser(function (user_id, done) {
    log.debug('deserializing user: ' + user_id);
    context.app.User.findOne({ _id: user_id }, (err, user) => {
      if(err) { 
        log.error('unable to retrieve user for deserialization with ID: ' + user_id);
        done(null, null);
      }
      done(null, user);
    })
  });

  // login endpoint
  context.app.post('/login',
    context.passport.authenticate('local',
      { // TODO success redirect should ideally preserve state and redirect back to starting page... maybe w/ flowstate?
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: 'Invalid username or password'
      })
  );

  // local strategy
  context.passport.use(new LocalStrategy(
    function (username, password, done) {
      log.debug('attempting to authenticate user: ' + username);
      context.app.User.findOne({ username: username, password: password }, (err, user) => {
        if (err) throw err;
        if (user === null) {
          log.info('Couldnt find matching DB record for user: ' + JSON.stringify(user));
          return done(null, false, { message: 'invalid username or password' });
        }

        log.info('successfully authd user: ' + user.username);
        return done(null, user);
      });
    }
  ));

  // google strategy
  context.app.get('/auth/google', context.passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));
  context.app.get('/auth/google/callback', context.passport.authenticate('google', { failureRedirect: '/login' }), (req, res) => res.redirect('/'));
  context.passport.use(new GoogleStrategy({
    clientID: process.env.GOOG_CLIENT_ID,
    clientSecret: process.env.GOOG_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/google/callback"
  }, function (accessToken, refreshToken, profile, done) {
    const googUsername = 'goog|' + profile.id;
    return done(undefined, { username: googUsername });
  }
  ));

  // github strategy
  context.app.get('/auth/github', context.passport.authenticate('github'));
  context.app.get('/auth/github/callback', context.passport.authenticate('github', { failureRedirect: '/login' }), (req, res) => res.redirect('/'));
  context.passport.use(new GitHubStrategy({
    clientID: process.env.GITHUB_CLIENT_ID,
    clientSecret: process.env.GITHUB_CLIENT_SECRET,
    callbackURL: "http://localhost:3000/auth/github/callback"
  },
    function (accessToken, refreshToken, profile, done) {
      const githubUsername = 'github|' + profile.id;
      return done(undefined, { username: githubUsername });
    }
  ));

  // authenticate clients for places like token endpoint
  context.passport.use(new ClientPasswordStrategy((client_id, clientSecret, done) => {
      log.debug('Attempting to perform client authentication...');
      context.app.Client.findOne({ client_id: client_id }, function (err, client) {
        if (err) { 
          log.error('error occurred attempting to lookup client by ID for credential validation');
          return done(err); 
        }
        if (!client) { 
          log.error('could not find client when attempting to validate credentials: ' + client_id)
          return done(null, false); 
        }

        log.debug('found client: ' + client_id + ', returning client object: ' + JSON.stringify(client));
        // TODO client secret validation
        //if (client.clientSecret != clientSecret) { return done(null, false); }
        return done(null, client);
      });
    }
  ));

  // bearer token auth
  context.passport.use(new BearerStrategy(
    function (token, done) {
      log.debug('attempting to retrieve access token: ' + token);
      context.app.AccessToken.findOne({ id: token }, function (err, accessToken) {
        if (err) { 
          log.error('error attempting to perform bearer auth: ' + err);
          return done(err); 
        }
        if (!accessToken) { 
          log.error('unable to retrieve access token for id: ' + accessToken);
          return done(null, false); 
        }

        context.app.User.findOne({ _id: accessToken.user_id }, function (err, user) {
          if (err) {
            log.error('error attempting to retrieve user for bearer auth: ' + err);
            return done(err);
          }
          if (!user) {
            log.error('unable to retrieve user for bearer auth by id: ' + accessToken.user_id);
            return done(null, false);
          }

          // TODO sanitize userinfo output, should not be returning _id
          return done(null, user, { scope: 'all' });
        });
      });
    }
  ));

  log.debug("Passport.js config complete");
  callback(context);
}
