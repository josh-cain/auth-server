'use strict'

const router = require('express').Router();
const log = require('../logger');

const pageTitle = 'Register New Account';

router.get('/', function(req, res, next) {
  res.render('pages/register', { title: pageTitle});
});

router.post('/', function(req, res, next) {
  // would ideally have **real** sanitation here
  if (!req.body.username || !req.body.password) {
    return res.render('pages/register', { title: pageTitle, error: 'Username and Password fields are required' });
  } 
  // TODO check context for req.app.User

  log.debug('Attempting to register user: ' + req.body.username);
  const newUser = new req.app.User({ username: req.body.username, password: req.body.password });
  newUser.save((err) => { 
    if (err) throw err;

    log.info('Successfully registered user: ' + newUser.username);
    // TODO communicate registration success to user
    return res.redirect('/login');
  });

});

module.exports = router;
