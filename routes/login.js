var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  console.error(req.flash());
  res.render('pages/login', { title: 'Auth Server' });
});

module.exports = router;
