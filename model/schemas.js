'use strict'

const mongoose = require('mongoose');

// TODO switch user references to user ID.
module.exports.userSchema = new mongoose.Schema({
  username:  {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});

module.exports.clientSchema = new mongoose.Schema({
  client_id:  {
    type: String,
    required: true,
    unique: true
  },
  name: {
    type: String,
    required: true,
    unique: true
  }
});

module.exports.authorizationCodeSchema = new mongoose.Schema({
  code:  {
    type: String,
    required: true,
    unique: true
  },
  client_id: {
    type: String,
    required: true
  },
  redirect_uri: {
    type: String,
    required: false
  },
  user_id: {
    type: String,
    required: true 
  },
  scope: {
    type: String,
    required: false
  }
});

module.exports.accessTokenSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true
  }, 
  user_id: {
    type: String,
    required: true
  },
  client_id: {
    type: String,
    required: true
  }
})