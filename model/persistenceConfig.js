'use strict'

const mongoose = require('mongoose');
const schemas = require('./schemas');
const log = require('../logger');

module.exports.init = (context, callback) => {
    log.debug('initializing Mongo...');

    const MongoMemoryServer = require('mongodb-memory-server').MongoMemoryServer;
    new MongoMemoryServer().getConnectionString()
        .then((connString) => {
            log.debug('MongoDB successfully initialized w/ conn string: ' + connString);

            const mongooseOpts = {
                // options for mongoose 4.11.3 and above
                autoReconnect: true,
                reconnectTries: Number.MAX_VALUE,
                reconnectInterval: 1000,
                useNewUrlParser: true
            };

            mongoose.connection.on('error', (e) => {
                if (e.message.code === 'ETIMEDOUT') {
                    mongoose.connect(mongoUri, mongooseOpts);
                }
            });

            mongoose.connect(connString, mongooseOpts, (err) => {
                log.debug(`Mongoose successfully connected to MongoDB at: ` + connString);

                // TODO look @ service locator implementations rather than just hanging all the things off 'app'
                const User = mongoose.model('User', schemas.userSchema);
                context.app.User = User;
                const AuthorizationCode = mongoose.model('AuthorizationCode', schemas.authorizationCodeSchema);
                context.app.AuthorizationCode = AuthorizationCode;
                const Client = mongoose.model('Client', schemas.clientSchema);
                context.app.Client = Client;
                const AccessToken = mongoose.model('AccessToken', schemas.accessTokenSchema);
                context.app.AccessToken = AccessToken;

                context.mongoose = mongoose;
                callback(context);
            });
        })
}
