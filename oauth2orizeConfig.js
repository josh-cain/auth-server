'use strict'

const oauth2orize = require('oauth2orize');
const login = require('connect-ensure-login');
const uid = require('uid');
const log = require('./logger');

module.exports.init = function (context, callback) {
  log.debug('Initializing Oauth2orize...');
  if (!context.app || !context.passport) {
    throw new Error('could not initialize oauth2orize, context app or passport elements not present');
  }
  var server = oauth2orize.createServer();

  // generate code
  server.grant(oauth2orize.grant.code(function (client, redirectURI, user, ares, done) {
    log.debug('Creating authz code grant for user: ' + JSON.stringify(user));
    const code = uid(12);
    const authorizationCode = new context.app.AuthorizationCode({ 
      code: code, 
      client_id: client.client_id, 
      redirect_uri: redirectURI, 
      user_id: user.id, 
      scope: ares.scope });
    authorizationCode.save((err) => {
      if (err) { log.error('OH NO!  authz code didnt get saved!' + err); }
      done(null, code);
    })
  }));

  // exchange code -> token
  server.exchange(oauth2orize.exchange.code(function (client, code, redirectURI, done) {
    log.debug('Exchanging OAuth code ' + code + ' for token.  retrieved client: ' + JSON.stringify(client));
    context.app.AuthorizationCode.findOne({ code: code }, (err, authorizationCode) => {
      if (err) { 
        log.error('unable to lookup code: ' + code);
        return done(err);
      }
      if (!authorizationCode) {
        log.error('could not retrieve authorization code: ' + code);
        // TODO fail faster?
        return done(null, null);
      }

      if (client.client_id !== authorizationCode.client_id) { 
        log.error('client_id presented for code to token exchange did not match client_id for code request:' + client.client_id + "/" + authorizationCode.client_id)
        return done(null, false); 
      }
      // TODO redirectURI validation

      log.debug('code -> token exchange success, issueing access token');
      const tokenId = uid(16);
      const accessToken = new context.app.AccessToken({ id: tokenId, user_id: authorizationCode.user_id, client_id: client.client_id });
      accessToken.save((error) => {
        log.debug('successfully saved access token: ' + tokenId);
        // TODO need to invalidate authz code so it can't be re-used
        done(null, tokenId);
      })
    })
  }));

  // serialization
  server.serializeClient(function (client, done) {
    log.debug('Serializing client: ' + client.client_id);
    return done(null, client.client_id);
  });
  server.deserializeClient(function (client_id, done) {
    log.debug('deserializing client: ' + client_id);
    context.app.Client.findOne({ client_id: client_id }, (err, client) => {
      if (err) { return done(err); }
      if (client === null) {
        log.error('Unable to retrieve client serialized with id: ' + id);
        return (done, null);
      }

      log.debug('found client, returning deserialized entity: ' + JSON.stringify(client));
      return done(null, client);
    })
  });

  // TODO prettify these end urls... why 'dialog'?
  context.app.get('/dialog/authorize', login.ensureLoggedIn(), server.authorize(function (clientID, redirectURI, done) {
    log.debug('Attempting to begin the OAuth flow with the end user (/authorize) with client_id: ' + clientID + ' and redirect_uri: ' + redirectURI);
    context.app.Client.findOne({ client_id: clientID }, (err, client) => {
      if (err) { return done(err); }
      if (!client) { return done(null, false); }
      // TODO redirect URI checking
      return done(null, client, redirectURI);
    })
  }), function (req, res) {
    log.debug('Rendering consent page...');
    res.render('pages/consent', {
      transactionID: req.oauth2.transactionID,
      user: req.user, client: req.oauth2.client
    });
  });

  // authz decision
  context.app.post('/dialog/authorize/decision',
    login.ensureLoggedIn(),
    server.decision());

  // code -> token exchange
  context.app.post('/token',
    context.passport.authenticate('oauth2-client-password', { session: false }),
    server.token(),
    server.errorHandler());

  // userinfo endpoint
  context.app.get('/auth/userinfo',
    context.passport.authenticate('bearer', { session: false }),
    function (req, res) {
      res.json(req.user);
    });

  context.server = server;
  log.debug('oauth2orize init complete.');
  callback(context);
};
