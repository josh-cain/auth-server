const winston = require('winston');

module.exports = winston.createLogger({
  level: 'debug',
  format: winston.format.cli(),
  defaultMeta: {},
  transports: [ new winston.transports.Console({
    stringify: (obj) => JSON.stringify(ojb)
  }) ]
});

