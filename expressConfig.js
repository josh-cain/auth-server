'use strict'

const path = require('path');
const express = require('express');
const passport = require('passport');

const log = require('./logger');

module.exports.init = function (context, callback) {
  log.debug('initializing express...');

  const app = express();
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'ejs');

  app.use(require('morgan')('dev'));
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(require('cookie-parser')());
  app.use(require('body-parser').urlencoded({ extended: false }));
  app.use(require('express-session')({ secret: 'missiles at midnight', resave: false, saveUninitialized: false }));

  // passport
  // TODO see if we can move this to passport init config...
  app.use(require('connect-flash')());
  app.use(passport.initialize());
  app.use(passport.session());

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('pages/error');
  });

  // normal routes
  app.use('/', require('./routes/index'));
  app.use('/login', require('./routes/login'));
  app.use('/register', require('./routes/register'));
  app.use('/account', require('connect-ensure-login').ensureLoggedIn(), require('./routes/account'));
  app.use('/logout', require('./routes/logout'));

  context.app = app;
  context.passport = passport;
  log.debug('express route initialization complete');
  callback(context);
};
