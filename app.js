'use strict'

const expressConfig = require('./expressConfig');
const persistenceConfig = require('./model/persistenceConfig');
const passportConfig = require('./passportConfig');
const oauth2orizeConfig = require('./oauth2orizeConfig');
const seedDatabase = require('./seedDatabase');

require('dotenv').config();
const log = require('./logger');

expressConfig.init({}, (context) => {
  persistenceConfig.init(context, (context) => {
    passportConfig.init(context, (context) => {
      oauth2orizeConfig.init(context, (context) => {
        seedDatabase.init(context, (context) => {

          const port = process.env.PORT || '3000';
          context.app.listen(port, () => {
            log.info('server started on port: ' + port);
          });
        })
      })
    })
  })
})
